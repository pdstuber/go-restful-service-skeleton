package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
)

type greeting struct {
	Id       string `json:"id"`
	Greeting string `json:"greeting"`
}

func greetingForName(name string) greeting {
	return greeting{
		Id:       uuid.New().String(),
		Greeting: fmt.Sprintf("Hello, %s", name),
	}
}

func main() {
	r := gin.Default()
	r.GET("/greet/:name", func(c *gin.Context) {
		name := c.Params.ByName("name")
		c.JSON(http.StatusOK, greetingForName(name))
	})

	r.Run() // listen and serve on 0.0.0.0:8080
}
