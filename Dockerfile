FROM golang:alpine as BUILDER
RUN apk update && apk add --no-cache git ca-certificates
ENV GO111MODULE on
WORKDIR /usr/src/app
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN GOOS=linux go build .

FROM alpine:latest
ENV GIN_MODE=release
# Copy our static executable and root ca certificates from alpine.
COPY --from=BUILDER /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=BUILDER /usr/src/app/go-restful-service-skeleton /bin/
ENV SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
ENV SSL_CERT_DIR=/etc/ssl/certs
ENTRYPOINT ["go-restful-service-skeleton"]
